# Getting Started
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
[Nodejs](https://nodejs.org/en/) should be installed.

### `npm run start` Start the app in dev mode 
### `npm run build` This compiles and optimize the app to be ready for production
After you have build the project, copy the generated `build-folder` to the root directory of the Python backend server.
